/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StyleSheet, Text } from 'react-native';
import {
  Colors
} from 'react-native/Libraries/NewAppScreen';
import RouterController from './Router/RouterController';


const App = () => {
  return (

    <RouterController />

  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
});

export default App;
