import { Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from '../pages/HomeScreen';
import MessagesScreen from '../pages/MessagesScreen';
import HelpScreen from '../pages/HelpScreen';
import DrawerMenu from './DrawerMenu';


const { width: screenWidth, height: screenHeight } = Dimensions.get('window');



const HomeNavigator = createStackNavigator({
    HomeScreen,
},
    {
        initialRouteName: 'HomeScreen',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#f7f7f7'
            },
            headerTintColor: 'gray',
            gestureEnabled: false,
            drawerLockMode: 'locked-closed',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            layout: {
                orientation: ["portrait"],
            },
        },
    });


const MessageNavigator = createStackNavigator({
    MessagesScreen,
}, {
        initialRouteName: 'MessagesScreen',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#f7f7f7'
            },
            headerTintColor: 'gray',
            gestureEnabled: false,
            drawerLockMode: 'locked-closed',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            layout: {
                orientation: ["portrait"],
            },
        },
        headerMode: 'none',

    
})

const HelpNavigator = createStackNavigator({
    HelpScreen,
}, {
            initialRouteName: 'HelpScreen',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#f7f7f7'
            },
            headerTintColor: 'gray',
            gestureEnabled: false,
            drawerLockMode: 'locked-closed',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            layout: {
                orientation: ["portrait"],
            },
            
        },
        headerMode: 'none',
})

const DrawerNavigator = createDrawerNavigator({
    HomeScreen: HomeNavigator,
    MessagesScreen : MessageNavigator,
    HelpScreen : HelpNavigator,
}, {
    initialRouteName: 'HomeScreen',
    drawerBackgroundColor: 'red',
    contentComponent: DrawerMenu,
    drawerLockMode: "locked-closed",
    disableGestures: true,
    drawerWidth: screenWidth - 60,
    edgeWidth: 0,
    contentOptions: {
        activeTintColor: 'yellow',
    },
    layout: {
        orientation: ["portrait"],
    },
});


export default RouterController = createAppContainer(DrawerNavigator);

