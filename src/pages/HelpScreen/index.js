import React, { Component } from 'react'
import { Dimensions, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'


const { height, width } = Dimensions.get('window');

class HelpScreen extends Component {
    static navigationOptions = {
        header: null
    }
    render(){ 
        return (
            <SafeAreaView>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => this.onMenuPress()}>
                            < MaterialCommunityIcons name='menu' size={35} style={{ paddingLeft: 10, color: 'blue' }} />
                        </TouchableOpacity>
                        <Text style={styles.headerText}>Help Screen</Text>
                    </View>
                    <View  style={ styles.containerContent}>
                        <Text> Ini isi Help Scren</Text>
                    </View>

                </View>
            </SafeAreaView>
        )
    }

    onMenuPress() { 
        this.props.navigation.openDrawer();
    }
}

export default HelpScreen

const styles = StyleSheet.create({
    container: {
        height: height,
        width: width,
        backgroundColor: '#fff',
    },
    header: {
        marginTop: 30,
        flexDirection: "row",
        alignItems: 'center',
    },
    headerText: {
        fontSize: 20,
        fontWeight: '500',
        paddingLeft: 10,
    },
    containerContent: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    }

})
