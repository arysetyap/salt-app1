import React,{ Component } from 'react'
import { Dimensions, SafeAreaView, StatusBar, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import MaterialComunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'


const { height, width } = Dimensions.get('window');

class MessageScreen extends Component { navigationOptions = {
        header : null
}
    
    render() { 
        return (
            <SafeAreaView>
                <StatusBar barStyle='default' />
                <View style={ styles.container}>
                    <View style={styles.header }>
                        <TouchableOpacity onPress={() => this.onMenuPress()}>
                            <MaterialComunityIcons name='menu' size={35} style={{paddingLeft:10, color: 'blue'}} />
                        </TouchableOpacity>
                        <Text style={styles.headerText}> Message Screen</Text>
                    </View>
                    <View style={ styles.containerContent}>
                        <Text> Ini isi Message Screen</Text>
                    </View>
                </View>
           </SafeAreaView>
        )
        
    }

    onMenuPress() { 
        this.props.navigation.openDrawer();
    }
}

export default MessageScreen

const styles = StyleSheet.create({
    container: {
        height: height,
        width: width,
        backgroundColor: '#fff',
    },
    header: {
        marginTop: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize: 20,
        fontWeight: '500',
        paddingLeft: 10
    },
    containerContent: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    }
})
